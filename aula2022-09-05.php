<?php

class Pessoa {
    public $nome;

    function ___construct(string $nome){
        $this->nome = $nome;
    }
    function ___toString() : string {
        return "Pessoa {nome: $this->nome}";
    }
}

$pessoa1 = new Pessoa ('Camila Yamauchi');

echo $pessoa1, "\n";
