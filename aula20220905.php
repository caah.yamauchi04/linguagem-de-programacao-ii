<?php

class Atividade {
    public $materia;
    public $dataInicio;
    public $dataEntrega;

    function __construct(string $materia, string $dataInicio, string $dataEntrega){
        $this->materia = $materia;
        $this->dataInicio = $dataInicio;
        $this->dataEntrega = $dataEntrega;
    }
    function __toString() : string {
        return "Atividade Cadastrada com Sucesso {Materia: $this->materia,Data de Inicio: $this->dataInicio,Data de Entrega: $this->dataEntrega}";
    }
}

class Prova {
    public $materia;
    public $dataProva;

    function __construct(string $materia, string $dataProva){
        $this->materia = $materia;
        $this->dataProva = $dataProva;
    }
    function __toString() : string {
        return "Prova Realizada! {Materia: $this->materia,Data da Prova:$this->dataProva}";
    }
}

$Atividade1 = new Atividade ('Banco de Dados','2022-09-02', '2022-09-09');
echo $Atividade1,"\n";
$Prova1 = new Prova ('Linguagem de Programacao','2022-10-03');
echo $Prova1, "\n";

